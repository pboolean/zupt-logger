# README #
This java application was built with NetBeans. and Java 8.
To run it, simply copy the /dist directory to your computer and run 

`java -jar "ZUPT Visualizer.jar"`

To use it, first connect to a serial port (e.g. on com13):

`connect com13`

At this point you should be receiving data. To start logging, type

`startOutput <filename>`

where `<filename>` is a filename (such as `coolData`)

To quit at any time, simply type 

`quit`
