/*
 * The MIT License
 *
 * Copyright 2015 Paul.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package arduimu;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paul
 */
public class ImuSensorPacket extends BasicSensorPacket
{

    public final Quaternion orientation;
    public static final double SCALE_FACTOR = 16384.0f;

    public ImuSensorPacket(String[] fields)
    {
        super(fields);

        float x, y, z, w;
        w = rawDataToFloat(fields[HEADER_SIZE_TEXT + 0]);
        x = rawDataToFloat(fields[HEADER_SIZE_TEXT + 1]);
        y = rawDataToFloat(fields[HEADER_SIZE_TEXT + 2]);
        z = rawDataToFloat(fields[HEADER_SIZE_TEXT + 3]);

        Quaternion quaternionMag = new Quaternion(x, y, z, w);
        quaternionMag = quaternionMag.normalized();

        orientation = new Quaternion(
                quaternionMag.x,
                quaternionMag.y,
                quaternionMag.z,
                quaternionMag.w);
    }

    public ImuSensorPacket(byte[] rawData)
    {
        super(rawData); // process header

        double x = -1f, y = -1f, z = -1f, w = -1f;

        byte[] wBytes = new byte[2];
        byte[] xBytes = new byte[2];
        byte[] yBytes = new byte[2];
        byte[] zBytes = new byte[2];
        
        try
        {
            System.arraycopy(rawData, HEADER_SIZE_BINARY + 0 * 2, wBytes, 0, wBytes.length);
            System.arraycopy(rawData, HEADER_SIZE_BINARY + 1 * 2, xBytes, 0, xBytes.length);
            System.arraycopy(rawData, HEADER_SIZE_BINARY + 2 * 2, yBytes, 0, yBytes.length);
            System.arraycopy(rawData, HEADER_SIZE_BINARY + 3 * 2, zBytes, 0, zBytes.length);

            try
            {
                w = (double) ByteArrayHelpers.getSignedIntFromBytes(wBytes, false) / SCALE_FACTOR;
                x = (double) ByteArrayHelpers.getSignedIntFromBytes(xBytes, false) / SCALE_FACTOR;
                y = (double) ByteArrayHelpers.getSignedIntFromBytes(yBytes, false) / SCALE_FACTOR;
                z = (double) ByteArrayHelpers.getSignedIntFromBytes(zBytes, false) / SCALE_FACTOR;
            } catch (IllegalArgumentException ex)
            {
                Logger.getLogger(AccelSensorPacket.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (ArrayIndexOutOfBoundsException ex)
        {
            System.err.println(ex);
        }

        orientation = new Quaternion(x, y, z, w);
    }

    public String toString()
    {
        return "Time: " + timestamp + " ms"
                + "Orientation: "
                + " W = " + orientation.w
                + " X = " + orientation.x
                + " Y = " + orientation.y
                + " Z = " + orientation.z;

    }
}
