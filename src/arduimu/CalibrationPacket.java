/*
 * The MIT License
 *
 * Copyright 2015 Paul.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package arduimu;

/**
 *
 * @author Paul
 */
public class CalibrationPacket extends BasicSensorPacket
{
    
    /**
     * Scale from 0-3 where:
     * 0: uncalibrated
     * 3: fully callibrated
     */
    public final int systemCalibrationStatus;
    
    /**
     * Scale from 0-3 where:
     * 0: uncalibrated
     * 3: fully callibrated
     */
    public final int gyroscopeCalibrationStatus;
    
    /**
     * Scale from 0-3 where:
     * 0: uncalibrated
     * 3: fully callibrated
     */
    public final int accelerationCalibrationStatus;
    
    /**
     * Scale from 0-3 where:
     * 0: uncalibrated
     * 3: fully callibrated
     */
    public final int magnetometerCalibrationStatus;
    
    /**
     * contents of CALIB_STAT register
     */
    public final int CALIB_STAT_contents;

    public CalibrationPacket(String[] rawDataFields)
    {
        super(rawDataFields);
        
        CALIB_STAT_contents = Integer.parseInt(rawDataFields[HEADER_SIZE_TEXT + 0], 16);
        
        // no need to look at the rest of the data
        systemCalibrationStatus = (CALIB_STAT_contents >> 6) & 0x03; // bits 7:6
	gyroscopeCalibrationStatus = (CALIB_STAT_contents >> 4) & 0x03; // bits 5:4
	accelerationCalibrationStatus = (CALIB_STAT_contents >> 2) & 0x03; // bits 3:2
	magnetometerCalibrationStatus = (CALIB_STAT_contents >> 0) & 0x03; // bits 1:0
    }
    
    public CalibrationPacket(byte[] rawData)
    {
        super(rawData);
        
        CALIB_STAT_contents = Byte.toUnsignedInt(rawData[HEADER_SIZE_BINARY + 0]);
        
        systemCalibrationStatus = (CALIB_STAT_contents >> 6) & 0x03; // bits 7:6
	gyroscopeCalibrationStatus = (CALIB_STAT_contents >> 4) & 0x03; // bits 5:4
	accelerationCalibrationStatus = (CALIB_STAT_contents >> 2) & 0x03; // bits 3:2
	magnetometerCalibrationStatus = (CALIB_STAT_contents >> 0) & 0x03; // bits 1:0
    }
    
    public String toString()
    {
        return "CALIBRATION DATA:\n" +
                "Sensor ID: " + this.sensorID + "\n" +
                "Time: " + timestamp + "ms\n" +
                "Sys Cal: " + this.systemCalibrationStatus + "\n" + 
                "Gyro Cal: " + this.gyroscopeCalibrationStatus + "\n" + 
                "Acc Cal: " + this.accelerationCalibrationStatus + "\n" + 
                "Mag Cal: " + this.magnetometerCalibrationStatus + "\n" +
                "stat: " + this.CALIB_STAT_contents + "\n";
    }
}
