/*
 * The MIT License
 *
 * Copyright 2015 Paul White.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package arduimu;

/**
 *
 * @author Paul White
 */
public enum PacketVariant
{

    /**
     * Raw orientation data from an IMU sensor in the form of a quaternion
     */
    IMU_DATA('Q'),
    /**
     * Raw linear acceleration data from an accelerometer in the form of a
     * vector3.
     */
    ACCEL_DATA('A'),
    /**
     * Control data
     */
    CONTROL('C'),
    /**
     * Unrecognized format
     */
    UNKNOWN('X'),
    
    /**
     * Calibration status
     */
    CALIBRATION_STATUS('L');

    public final char value;

    private PacketVariant(char value)
    {
        this.value = value;
    }

    public static PacketVariant getPacketVariant(char value)
    {
        for (PacketVariant variant : PacketVariant.values())
        {
            if (value == variant.value)
            {
                return variant;
            }
        }
        // no value matched up
        return UNKNOWN;
    }

    /**
     * Decide which sensor packet to initialize based upon the variant.
     *
     * @param rawDataFields data received from serial port
     * @return an appropriate #BasicSensorPacket
     */
    public BasicSensorPacket initPacket(String[] rawDataFields)
    {
        BasicSensorPacket packet;
        switch (this)
        {
            case IMU_DATA:
                packet = new ImuSensorPacket(rawDataFields);
                break;

            case ACCEL_DATA:
                packet = new AccelSensorPacket(rawDataFields);
                break;
                
            case CALIBRATION_STATUS:
                packet = new CalibrationPacket(rawDataFields);
                break;

            default:
                packet = new BasicSensorPacket(rawDataFields);
                break;
        }

        return packet;
    }

    public BasicSensorPacket initPacket(byte[] rawData)
    {
        BasicSensorPacket packet;
        switch (this)
        {
            case IMU_DATA:
                packet = new ImuSensorPacket(rawData);
                break;

            case ACCEL_DATA:
                packet = new AccelSensorPacket(rawData);
                break;
                
            case CALIBRATION_STATUS:
                packet = new CalibrationPacket(rawData);
                break;

            default:
                packet = new BasicSensorPacket(rawData);
                break;
        }

        return packet;
    }

}
