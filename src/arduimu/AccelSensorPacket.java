/*
 * The MIT License
 *
 * Copyright 2015 Paul.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package arduimu;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paul
 */
public class AccelSensorPacket extends BasicSensorPacket
{

    public final Vector3 linearAcceleration;
    public static final double SCALE_FACTOR = 100.0f;

    public AccelSensorPacket(String[] rawData)
    {
        super(rawData); // process header

        float x, y, z;
        x = rawDataToFloat(rawData[HEADER_SIZE_TEXT + 0]);
        y = rawDataToFloat(rawData[HEADER_SIZE_TEXT + 1]);
        z = rawDataToFloat(rawData[HEADER_SIZE_TEXT + 2]);

        linearAcceleration = new Vector3(x, y, z);
    }

    public AccelSensorPacket(byte[] rawData)
    {
        super(rawData); // process header

        byte[] xBytes = new byte[2];
        System.arraycopy(rawData, HEADER_SIZE_BINARY + 0 * 2, xBytes, 0, xBytes.length);

        byte[] yBytes = new byte[2];
        System.arraycopy(rawData, HEADER_SIZE_BINARY + 1 * 2, yBytes, 0, yBytes.length);

        byte[] zBytes = new byte[2];
        System.arraycopy(rawData, HEADER_SIZE_BINARY + 2 * 2, zBytes, 0, zBytes.length);

        double x = -1f, y = -1f, z = -1f;

        try
        {
            x = (double) ByteArrayHelpers.getSignedIntFromBytes(xBytes, false) / SCALE_FACTOR;
            y = (double) ByteArrayHelpers.getSignedIntFromBytes(yBytes, false) / SCALE_FACTOR;
            z = (double) ByteArrayHelpers.getSignedIntFromBytes(zBytes, false) / SCALE_FACTOR;
        } catch (IllegalArgumentException ex)
        {
            Logger.getLogger(AccelSensorPacket.class.getName()).log(Level.SEVERE, null, ex);
        }

        linearAcceleration = new Vector3(x, y, z);
    }

    @Override
    public String toString()
    {
        return "Acceleration: X = " + linearAcceleration.x
                + " Y = " + linearAcceleration.y
                + " Z = " + linearAcceleration.z;
    }
}
