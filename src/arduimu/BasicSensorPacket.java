/*
 * The MIT License
 *
 * Copyright 2015 Paul.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package arduimu;

/**
 * Represent a packet of data that was received from a sensor.
 *
 * @author Paul
 */
public class BasicSensorPacket
{

    //------------------------
    // binary data constants
    //------------------------
    /**
     * splits packets in the incoming data stream
     */
    public static final String packetDelimiter = "\n";

    /**
     * Splits fields in a packet
     */
    public static final String fieldDelimiter = ",";

    /**
     * max amount of data that can be sent (header size + max payload)
     */
    public static final int MAX_PACKET_SIZE = BasicSensorPacket.HEADER_SIZE_BINARY + BasicSensorPacket.MAX_PAYLOAD_SIZE;

    public static final int HEADER_SIZE_BINARY = 8;
    
    public static final int MAX_PAYLOAD_SIZE = 255;

    //------------------------
    // text data constants
    //------------------------
    /**
     * Where in the packet does the payload data start?
     */
    public static final int HEADER_SIZE_TEXT = 4; // header size
    
    /**
     * Split packets in the serial stream
     */
    public static final String PACKET_DELIMITER = "\n";
    
    /**
     * split fields within a packet
     */
    public static final String FIELD_DELIMITER = ",";

    //---------------------
    // Instance Variables
    //---------------------
    /**
     * the unique ID of the sensor where this packet originated
     */
    public final int sensorID;

    /**
     * What is the variant of this packet? use this information to correctly
     * cast it.
     */
    public final PacketVariant variant;

    /**
     * Version of the protocol
     */
    public final int version;

    /**
     * raw data received over serial port (text)
     */
    public final String[] rawDataFields;

    /**
     * raw data received over serial port (binary)
     */
    public final byte[] rawDataBytes;

    /**
     * Timestamp (milliseconds)
     */
    public final long timestamp;

    /**
     * Call this function to parse a raw text stream into a packet. If this
     * function recognizes the packet's type, that packet will be parsed into
     * the correct type of packet (e.g. IMUPacket)
     *
     * @param rawData a single packet of data that was read from a serial port
     * @return
     */
    public static BasicSensorPacket parseData(String inputData)
    {
        String[] fields = inputData.split(FIELD_DELIMITER);
        BasicSensorPacket temp = new BasicSensorPacket(fields);
        BasicSensorPacket packet = temp.variant.initPacket(fields);
        
        return packet;
    }
    
    /**
     * Call this function to parse a raw byte stream into a packet. If this
     * function recognizes the packet's type, that packet will be parsed into
     * the correct type of packet (e.g. IMUPacket)
     *
     * @param rawData a single packet of data that was read from a serial port
     * @return
     */
    public static BasicSensorPacket parseData(byte[] rawData)
    {
        BasicSensorPacket packet = null;
        if (rawData.length >= HEADER_SIZE_BINARY) // full header
        {

            // byte 1 is the variant 
            PacketVariant mVariant;
            mVariant = PacketVariant.getPacketVariant((char) rawData[1]);
            
            if(PacketVariant.UNKNOWN == mVariant)
            {
                throw new IllegalArgumentException("Unrecognized packet: " + packet);
            }

            packet = mVariant.initPacket(rawData);

        }

        return packet;
    }

    /**
     * parse raw data fields and create a basic sensor packet
     *
     * @param rawData
     */
    BasicSensorPacket(byte[] rawData)
    {
        if (rawData.length >= HEADER_SIZE_BINARY)
        {
            // byte 0 is the version
            int mVersion = rawData[0];

            // byte 1 is the variant 
            PacketVariant mVariant;
            mVariant = PacketVariant.getPacketVariant((char) rawData[1]);

            // index 2 is the sensor ID
            int mSensorID = rawData[2];

            // bytes 3-6 are the timestamp, but they are big-endian so deal with that
            byte[] timestampBytes =
            {
                rawData[3],
                rawData[4],
                rawData[5],
                rawData[6]
            };

            long mTimestamp;
            try
            {
                mTimestamp = Integer.toUnsignedLong(ByteArrayHelpers.getUnsignedIntFromBytes(timestampBytes, false));
            } catch (IllegalArgumentException ex)
            {
                mTimestamp = -1;
            }
            this.sensorID = mSensorID;
            this.variant = mVariant;
            this.version = mVersion;
            this.timestamp = mTimestamp;
            this.rawDataBytes = rawData;
        } else
        {
            this.sensorID = -1;
            this.variant = PacketVariant.UNKNOWN;
            this.version = -1;
            this.timestamp = -1;
            this.rawDataBytes = rawData;
        }

        this.rawDataFields = new String[0];
    }

    /**
     * parse raw data fields and create a basic sensor packet
     *
     * @param rawDataFields
     */
    public BasicSensorPacket(String[] rawDataFields)
    {
        if (rawDataFields.length > 3) // version number, type, sensor ID ( at least 3 fields)
        {
            // index 0 is the version
            int mVersion = -1;
            try
            {
                mVersion = Integer.parseInt(rawDataFields[0]);
            } catch (NumberFormatException nfe)
            {
                throw new NumberFormatException(rawDataFields[0] + " could not be parsed as an integer. ");
            }

            // index 1 is the variant 
            PacketVariant mVariant;
            mVariant = PacketVariant.getPacketVariant(rawDataFields[1].charAt(0));

            // index 2 is the sensor ID
            int mSensorID = -1;
            try
            {
                mSensorID = Integer.parseInt(rawDataFields[2]);
            } catch (NumberFormatException nfe)
            {
                throw new NumberFormatException(rawDataFields[2] + " could not be parsed as an integer. ");
            }

            // index 3 is the timestamp
            long mTimestamp = -1;
            try
            {
                mTimestamp = Integer.parseInt(rawDataFields[3], 10);
            } catch (NumberFormatException nfe)
            {
                throw new NumberFormatException(rawDataFields[3] + " could not be parsed as an integer. ");
            }

            this.sensorID = mSensorID;
            this.variant = mVariant;
            this.version = mVersion;
            this.timestamp = mTimestamp;
            this.rawDataFields = rawDataFields;
        } else
        {
            this.sensorID = -1;
            this.variant = PacketVariant.UNKNOWN;
            this.version = -1;
            this.timestamp = -1;
            this.rawDataFields = rawDataFields;
        }

        this.rawDataBytes = new byte[0];
    }

    /// <summary>
    /// Convert an incoming data field to a float.
    /// </summary>
    /// <param name="rawData"></param>
    /// <returns></returns>
    /**
     * Convert an incoming data field to a float.
     *
     * @param inString a big-endian, binary-encoded float
     * @return the <code>float</code> representation of <code>inString</code>
     */
    public static float rawDataToFloat(String inString)
    {
        byte[] inData = new byte[4];

        if (inString.length() == 8)
        {
            inData[0] = (byte) Integer.parseInt(inString.substring(0, 2), 16);
            inData[1] = (byte) Integer.parseInt(inString.substring(2, 4), 16);
            inData[2] = (byte) Integer.parseInt(inString.substring(4, 6), 16);
            inData[3] = (byte) Integer.parseInt(inString.substring(6, 8), 16);
        }

        int intbits = (inData[3] << 24) | ((inData[2] & 0xff) << 16) | ((inData[1] & 0xff) << 8) | (inData[0] & 0xff);
        float dataFloat = Float.intBitsToFloat(intbits);

        return dataFloat;
    }
}
