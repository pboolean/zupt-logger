/*
 * The MIT License
 *
 * Copyright 2015 Paul.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package arduimu;

//import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;
//import logger.CsvLogger;

/**
 *
 * @author Paul
 */
public final class SensorReader
{
    
    public interface SensorReaderDelegate
    {
        public void didRecievepacket(BasicSensorPacket packet);
    }

    public static final int DEFAULT_BAUD = 115200;
    public static final String DEFAULT_PORT = "COM6";
    public static final int INPUT_TIMEOUT = 5000;

    /**
     * Allow the received sensor packets to be indexed by type and by name
     */
    private class SensorPacketDictionaryIndex
    {

        public int sensorID = -1;
        public PacketVariant sensorVariant = PacketVariant.UNKNOWN;

        @Override
        public int hashCode()
        {
            return sensorID + 2 * sensorVariant.value;
        }

        @Override
        public boolean equals(Object obj)
        {
            try
            {
                SensorPacketDictionaryIndex other = (SensorPacketDictionaryIndex) obj;
                return (this.sensorID == other.sensorID && this.sensorVariant == other.sensorVariant);
            } catch (ClassCastException e)
            {
                return false;
            } catch (NullPointerException npe)
            {
                return false;
            }
        }
    }

    public String serialPortID;
    public int baudRate;

//    private CsvLogger mCsvLogger;
//    private boolean logImmediate = false;

    /**
     * Store the last received sensor packet of each type from each sensor
     */
    private HashMap<SensorPacketDictionaryIndex, BasicSensorPacket> lastPacketsOfType;

    // Serial Communication
    SerialPort inputPort;
//    char[] fieldDelimiters =
//    {
//        ','
//    };
    String packetDelimiter = BasicSensorPacket.packetDelimiter;
    boolean isAlive = true;
    Thread serialThread;
    private SensorReaderDelegate delegate;

    /**
     * Default constructor, need to set serialPortID and baudRate, then call
     * openSerialPort() to make it do things
     */
    public SensorReader()
    {
        lastPacketsOfType = new HashMap<>();
    }

//    /**
//     * Construct a SensorReader that will automatically log sensor data to a
//     * file
//     *
//     * @param outputFile
//     */
//    public SensorReader(File outputFile) throws IOException
//    {
//        setOutputFile(outputFile);
//    }

//    public void setOutputFile(File outputFile) throws IOException
//    {
//        String filePath = outputFile.getPath();
//        String fileName = outputFile.getName();
//        String fileExtension = "csv";
//
//        String header = "Sample Time (millis),Time (millis),AccelX (m/s^2),AccelY (m/s^2),AccelZ (m/s^2),OrientationW,OrientationX,OrientationY,OrientationZ";
//        mCsvLogger = new CsvLogger();
//        mCsvLogger.writeHeader(filePath, fileName, fileExtension, CsvLogger.FileCollisionHandlerMode.FILENAME_NUMBER, header);
//        logImmediate = true;
//    }
    
    public void setDelegate(SensorReaderDelegate mDelegate)
    {
        this.delegate = mDelegate;
    }

//    public void clearOutputFile()
//    {
//        logImmediate = false;
//        mCsvLogger = null;
//    }

    /**
     * Convenience constructor that automatically starts the serial port
     *
     * @param serialPortID
     * @param baudRate
     */
    public SensorReader(String serialPortID, int baudRate)
    {
        this();
        this.serialPortID = serialPortID;
        this.baudRate = baudRate;

        openSerialPort();
    }
    
    public SensorReader(String serialPortID, int baudRate, SensorReaderDelegate delegate)
    {
        this(serialPortID, baudRate);
        setDelegate(delegate);
    }

//    public SensorReader(String serialPortID, int baudRate, File outputFile) throws IOException
//    {
//        this(outputFile);
//        this.serialPortID = serialPortID;
//        this.baudRate = baudRate;
//
//        openSerialPort();
//    }

    /**
     * Open the serial port if it isn't already
     *
     * @param serialPortID
     * @param baudRate
     */
    public void init(String serialPortID, int baudRate)
    {
        if (null != inputPort)
        {
            if (!inputPort.isOpened())
            {
                this.serialPortID = serialPortID;
                this.baudRate = baudRate;

                openSerialPort();
            }
        } else
        {
            this.serialPortID = serialPortID;
            this.baudRate = baudRate;

            openSerialPort();
        }
    }

    /**
     * Open a new serial port, after first closing any open serial ports.
     */
    public void openSerialPort()
    {
        try
        {
            // housekeeping
            if (null != inputPort && inputPort.isOpened())
            {
                inputPort.closePort();
            }

            while (null != serialThread && serialThread.isAlive()) // wait for thread to stop if it's running
            {
                isAlive = false;
            }

            // Let's go
            isAlive = true;

            inputPort = new SerialPort(this.serialPortID); // replace with code to allow user to initialize, from a configuration

            inputPort.openPort();
            inputPort.setParams(
                    this.baudRate,
                    8,
                    1,
                    0);
            /**
             * read the serial port until told to stop
             */
            Runnable readSerialRunnable = new Runnable()
            {

                @Override
                public void run()
                {
                    Date startDate, stopDate;
                    long startTime, stopTime;

                    boolean isInSync = false;

                    while (isAlive)
                    {
                        if (!isInSync)
                        {
                            try {
                                System.err.println("Out of sync. syncing...");
                                findStartOfBinaryPacket(inputPort);
                                isInSync = true;
                                System.err.println("back in sync.");
                            } catch (IOException ex) {
                                System.err.println("Serial exception. re-sycning. " + ex);
                                isInSync = false;
                                continue; // start again yo
                            }
                        }

                        startDate = new Date();
                        startTime = startDate.getTime();
                        byte[] inputData;
//                        String inputData;
                        try
                        {
                            inputData = serialReadBinaryPacket(inputPort);
//                            inputData = serialReadTextPacket(inputPort);
                            BasicSensorPacket packet = BasicSensorPacket.parseData(inputData);
                            SensorPacketDictionaryIndex index = new SensorPacketDictionaryIndex();
                            if (null != packet)
                            {
                                index.sensorID = packet.sensorID;
                                index.sensorVariant = packet.variant;
                                lastPacketsOfType.put(index, packet);
                            }
                            
                            if (null != delegate)
                            {
                                delegate.didRecievepacket(packet);
                            }

                            // this should be removed shortly after being replaced by the delegate
//                            if (logImmediate && (PacketVariant.ACCEL_DATA == packet.variant))
//                            {
//                                AccelSensorPacket accelPacket = (AccelSensorPacket) packet;
//
//                                SensorPacketDictionaryIndex imuIndex = new SensorPacketDictionaryIndex();
//
//                                imuIndex.sensorID = packet.sensorID;
//                                imuIndex.sensorVariant = PacketVariant.IMU_DATA;
//
//                                ImuSensorPacket imuPacket = (ImuSensorPacket) lastPacketsOfType.get(imuIndex);
//                                long dataTime = (new Date()).getTime();
//
//                                String logData
//                                        = dataTime + ","
//                                        + accelPacket.timestamp + ","
//                                        + accelPacket.linearAcceleration.x + ","
//                                        + accelPacket.linearAcceleration.y + ","
//                                        + accelPacket.linearAcceleration.z + ","
//                                        + imuPacket.orientation.w + ","
//                                        + imuPacket.orientation.x + ","
//                                        + imuPacket.orientation.y + ","
//                                        + imuPacket.orientation.z;
//
//                                mCsvLogger.writeLogLine(logData);
//                            }
                            
//                            if(PacketVariant.CALIBRATION_STATUS == packet.variant)
//                            {
//                                System.out.println(packet);
//                            }

//                            System.out.println("Received Time: " + packet.timestamp + "ms");
//                            stopDate = new Date();
//                            stopTime = stopDate.getTime();
//                        } catch (IOException ex)
//                        {
//                            Logger.getLogger(SensorReader.class.getName()).log(Level.SEVERE, null, ex);
//                            isInSync = false;
////                        } catch (InvalidArgumentException ex)
////                        {
////                            Logger.getLogger(SensorReader.class.getName()).log(Level.SEVERE, null, ex);
////                            isInSync = false;
                        } catch (Exception ex)
                        {
                            System.err.println("Exception caught. re-syncing.");
                            ex.printStackTrace();
                            isInSync = false;
                        }

                    }
                }
            };

            serialThread = new Thread(readSerialRunnable, "Serial Port Reader");

            serialThread.start();

        } catch (SerialPortException ex)
        {
            Logger.getLogger(SensorReader.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * close the serial port down. this MUST be called at some point or the
     * Serial Port thread will continue to run until the device is unplugged.
     */
    public void close()
    {
        isAlive = false;
        if (null != inputPort)
        {
            try
            {
                inputPort.closePort();
                inputPort = null;

            } catch (SerialPortException ex)
            {
                Logger.getLogger(SensorReader.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Retrieve the last received packet of a particuluar type from a particular
     * sensor, or null if no packet of that type has been received
     *
     * @param sensorID the id of the sensor we are interested in
     * @param variant which type of packet are we interested in
     * @return
     */
    public BasicSensorPacket getLastPacket(int sensorID, PacketVariant variant)
    {
        BasicSensorPacket p;

        SensorPacketDictionaryIndex index = new SensorPacketDictionaryIndex();
        index.sensorID = sensorID;
        index.sensorVariant = variant;

        if (!lastPacketsOfType.containsKey(index))
        {
            p = null;
        } else
        {
            p = lastPacketsOfType.get(index);
        }

        return p;
    }

    /**
     * Read in a single packet from the serial stream, where the packet is
     * delimited as described in BasicSensorPacket
     *
     * @return a single packet's worth of text from the serial stream
     *
     * @throws IOException if there is an error reading from the serial port
     */
    private static String serialReadTextPacket(SerialPort inputPort) throws IOException
    {
        byte[] inputBuffer = new byte[BasicSensorPacket.MAX_PACKET_SIZE];
        int index = 0;

        // read until we hit a packet delimiter
        try
        {
            // block until there's something to read
            while (inputPort.getInputBufferBytesCount() == 0);

            byte[] inputByte = inputPort.readBytes(1, INPUT_TIMEOUT);

            while (index < BasicSensorPacket.MAX_PACKET_SIZE && (inputByte[0] != (byte) BasicSensorPacket.packetDelimiter.charAt(0)))
            {
                inputBuffer[index] = inputByte[0];
                inputByte = inputPort.readBytes(1, INPUT_TIMEOUT);
                index++;
            }

        } catch (SerialPortException ex)
        {
            Logger.getLogger(SensorReader.class.getName()).log(Level.SEVERE, "Serial exception... continuing.", ex);
            throw new IOException("serial error");
        } catch (SerialPortTimeoutException ex)
        {
            Logger.getLogger(SensorReader.class.getName()).log(Level.SEVERE, "Serial timeout exception... continuing", ex);
            throw new IOException("serial error");
        }

        String inputString = new String(inputBuffer).trim();

        return inputString;
    }

    /**
     * Read in a single packet from the serial stream, where the packet is
     * delimited as described in BasicSensorPacket
     *
     * @param inputPort
     * @returna single packet's worth of data from the serial stream, or an
     * empty byte array if a serial port exception occurred
     * @throws IOException if there is an error reading from the serial port
     */
    private static byte[] serialReadBinaryPacket(SerialPort inputPort) throws IOException
    {
        int dataSize = -69;

        byte[] receivedPacket = new byte[0];
        try
        {
            // block until there's something to read
            while (inputPort.getInputBufferBytesCount() == 0);

            byte[] header = inputPort.readBytes(8, INPUT_TIMEOUT);

            dataSize = Byte.toUnsignedInt(header[7]); // index of payload size

            byte[] data = inputPort.readBytes(dataSize, INPUT_TIMEOUT);
            //inputPort.readBytes(1, INPUT_TIMEOUT); // get the '\n' at the beginning of the next packet
            receivedPacket = ByteArrayHelpers.concat(header, data);

        } catch (SerialPortException ex)
        {
            Logger.getLogger(SensorReader.class.getName()).log(Level.SEVERE, "Serial exception... continuing.", ex);
            throw new IOException("serial error");
        } catch (SerialPortTimeoutException ex)
        {
            Logger.getLogger(SensorReader.class.getName()).log(Level.SEVERE, "Serial timeout exception... continuing", ex);
            throw new IOException("serial error");
        } catch (NegativeArraySizeException ex)
        {
            Logger.getLogger(SensorReader.class.getName()).log(Level.SEVERE, "Negative array size exception (" + dataSize + ")... continuing", ex);
            throw new IOException("serial error");
        }

        return receivedPacket;
    }

    /**
     * scan the serial input stream so that a subsequent read will be at the
     * beginning of a packet
     *
     * @param inputPort
     */
    private static final void findStartOfBinaryPacket(SerialPort inputPort) throws IOException
    {
        byte[] theByte;
        try
        {
            theByte = inputPort.readBytes(1, INPUT_TIMEOUT);

            while (PacketVariant.getPacketVariant((char) theByte[0]) == PacketVariant.UNKNOWN) // wait until we find a packet
            {
                theByte = inputPort.readBytes(1, INPUT_TIMEOUT);
            }
            // part of a header, read the rest of the header
            byte[] data = inputPort.readBytes(6, INPUT_TIMEOUT);
            int dataSize = Byte.toUnsignedInt(data[5]); // how much data?

            byte[] dummyData = inputPort.readBytes(dataSize, INPUT_TIMEOUT); // advance along
            int x = dummyData.length;

        } catch (SerialPortException ex)
        {
            throw new IOException("SerialPort Exception found" + ex);
        } catch (SerialPortTimeoutException ex)
        {
            throw new IOException("SerialPortTimeout Exception found" + ex);
        }
    }
}
