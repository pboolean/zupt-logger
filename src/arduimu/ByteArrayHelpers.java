package arduimu;

import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * The MIT License
 *
 * Copyright 2015 Paul White.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/**
 *
 * @author Paul
 */
public class ByteArrayHelpers
{

    public enum ByteArrayMode
    {

        BYTE_ARRAY_MODE_HEX,
        BYTE_ARRAY_MODE_DEC
    }

    public static int fromByteArray(byte[] bytes, boolean isBigEndian)
    {
        if (isBigEndian)
        {
            return bytes[0] << 24 | (bytes[1] & 0xFF) << 16 | (bytes[2] & 0xFF) << 8 | (bytes[3] & 0xFF);
        } else
        {
            return bytes[3] << 24 | (bytes[2] & 0xFF) << 16 | (bytes[1] & 0xFF) << 8 | (bytes[0] & 0xFF);
        }
    }

    public static String getByteArrayString(byte[] bytes)
    {
        return getByteArrayString(bytes, ByteArrayMode.BYTE_ARRAY_MODE_HEX);
    }

    public static String getByteArrayString(byte[] bytes, ByteArrayMode mode)
    {
        StringBuilder sBuilder = new StringBuilder();

        for (byte b : bytes)
        {
            switch (mode)
            {
                case BYTE_ARRAY_MODE_HEX:
                    sBuilder.append(String.format("0x%x ", b));
                    break;

                case BYTE_ARRAY_MODE_DEC:
                    sBuilder.append(String.format("%d ", b));
                    break;

                default:
                    break;
            }
        }
        return sBuilder.toString();
    }

    /**
     * parse an array of bytesBigEndian into an integer.
     *
     * @param bytes component bytesBigEndian of the desired integer
     * @return the integer representation of the transmitted value. use the
     * Integer wrapper class to handle unsigned integers.
     */
    public static int getUnsignedIntFromBytes(byte[] bytes, boolean isBigEndian) throws IllegalArgumentException
    {
        int result = 0;

        if (bytes.length <= 4)
        {

            for (int index = 0; index < bytes.length; index++)
            {
                int indexMod = index;
                if (isBigEndian)
                {
                    indexMod = bytes.length - index - 1;
                }

                int val = toUnsignedInt(bytes[index]) & 0xff;
                int inc = (val) << (8 * indexMod);
                result |= inc;
            }
        } else
        {
            throw new IllegalArgumentException("argument 'bytes' must be of length 4 or less");
        }
        return result;
    }

    /**
     * parse an array of bytes into an integer.
     *
     * @param bytes component bytesBigEndian of the desired integer
     * @return the integer representation of the transmitted value. use the
     * Integer wrapper class to handle unsigned integers.
     */
    public static int getSignedIntFromBytes(byte[] bytes, boolean isBigEndian) throws IllegalArgumentException
    {
        int result = 0;

        if (bytes.length <= 4)
        {

            result = getUnsignedIntFromBytes(bytes, isBigEndian);

            if (bytes.length < 4)
            {
                if (!isBigEndian && bytes[bytes.length - 1] < 0 // fix little-endian sign
                        || isBigEndian && bytes[0] < 0)         // fix big-endian sign
                {
                    result = (result - 1) - ((1 << (bytes.length * 8)) - 1);
                }
            }

        } else
        {
            throw new IllegalArgumentException("argument 'bytes' must be of length 4 or less");
        }
        return result;
    }

    /**
     * parse an int into its four constituent bytes
     *
     * @param x
     * @param isBigEndian
     * @return a byte array containing the 4-bytes represented by x.
     */
    public static final byte[] getBytesFromInt(int x, boolean isBigEndian)
    {
        byte[] bytes = new byte[4];

        for (int index = 0; index < bytes.length; index++)
        {
            int indexMod = index;
            if (isBigEndian)
            {
                indexMod = bytes.length - index - 1;
            }

            bytes[index] = (byte) ((x >> indexMod * 8) & 0xff);
        }

        return bytes;
    }

    public static final int toUnsignedInt(byte x)
    {
        int val = (int) x;
        if (val < 0)
        {
            val += 256; // java likes to automatically 2's complement
        }

        return val;
    }

    /**
     * Convert an incoming data field to a float.
     *
     * @param inString a big-endian, binary-encoded float
     * @return the <code>float</code> representation of <code>inString</code>
     */
    public static float rawDataToFloat(String inString)
    {
        byte[] inData = new byte[4];

        if (inString.length() == 8)
        {
            inData[0] = (byte) Integer.parseInt(inString.substring(0, 2), 16);
            inData[1] = (byte) Integer.parseInt(inString.substring(2, 4), 16);
            inData[2] = (byte) Integer.parseInt(inString.substring(4, 6), 16);
            inData[3] = (byte) Integer.parseInt(inString.substring(6, 8), 16);
        }

        float dataFloat = bytesToFloat(inData, true);

        return dataFloat;
    }

    /**
     * convert a 4-byte representation of a float to a <code>float</code>
     * primitive
     *
     * @param bytes
     * @param isBigEndian
     * @return the float represented by <code>bytesBigEndian</code>
     */
    public static float bytesToFloat(byte[] bytes, boolean isBigEndian)
    {
        if (bytes.length != 4)
        {
            throw new IllegalArgumentException("bytes.length must be 4, not " + bytes.length);
        }

        int intbits = getUnsignedIntFromBytes(bytes, isBigEndian);
        float dataFloat = Float.intBitsToFloat(intbits);

        return dataFloat;
    }

    public static byte[] concat(byte[] A, byte[] B)
    {
        int aLen = A.length;
        int bLen = B.length;
        byte[] C = new byte[aLen + bLen];
        System.arraycopy(A, 0, C, 0, aLen);
        System.arraycopy(B, 0, C, aLen, bLen);
        return C;
    }

    public static void main(String[] args)
    {
        System.out.println("ByteArrayHelpers main");
        try
        {
            byte[] bytesBigEndian = new byte[]
            {
                (byte) 0xde,
                (byte) 0xad,
                (byte) 0xbe,
                (byte) 0xef,
            };

            byte[] bytesLittleEndian = new byte[]
            {
                (byte) 0xef,
                (byte) 0xbe,
                (byte) 0xad,
                (byte) 0xde,
            };

            int bigEndian = getUnsignedIntFromBytes(bytesBigEndian, true);
            int littleEndian = getUnsignedIntFromBytes(bytesLittleEndian, false);

            System.out.printf("big_endian: %d (%x)\n", bigEndian, bigEndian);
            System.out.printf("little_endian: %d (%x)\n", littleEndian, littleEndian);
            int positive = 12;
            int negative = -12;

            byte[] positiveBytes = getBytesFromInt(positive, false);
            byte[] negativeBytes = getBytesFromInt(negative, false);
            byte[] negativeByte = new byte[]{negativeBytes[0]};
            int signedPos = getSignedIntFromBytes(positiveBytes, false);
            int signedNeg = getSignedIntFromBytes(negativeByte, false);
            int unsignedPos = getUnsignedIntFromBytes(positiveBytes, false);
            int unsignedNeg = getUnsignedIntFromBytes(negativeByte, false);

            System.out.printf("Signed: %d (%x) -> %s -> %d (%x)\n",
                    positive, positive,
                    getByteArrayString(positiveBytes, ByteArrayMode.BYTE_ARRAY_MODE_HEX),
                    signedPos, signedPos);

            System.out.printf("Unsigned: %d (%x) -> %s -> %d (%x)\n",
                    positive, positive,
                    getByteArrayString(positiveBytes, ByteArrayMode.BYTE_ARRAY_MODE_HEX),
                    unsignedPos, unsignedPos);

            System.out.printf("Signed: %d (%x) -> %s -> %d (%x)\n",
                    negative, negative,
                    getByteArrayString(negativeByte, ByteArrayMode.BYTE_ARRAY_MODE_HEX),
                    signedNeg, signedNeg);

            System.out.printf("Unsigned: %d (%x) -> %s -> %d (%x)\n",
                    negative, negative,
                    getByteArrayString(negativeByte, ByteArrayMode.BYTE_ARRAY_MODE_HEX),
                    unsignedNeg, unsignedNeg);

        } catch (IllegalArgumentException ex)
        {
            Logger.getLogger(ByteArrayHelpers.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
