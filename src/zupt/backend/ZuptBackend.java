/*
 * The MIT License
 *
 * Copyright 2015 Paul White.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package zupt.backend;

import arduimu.AccelSensorPacket;
import arduimu.BasicSensorPacket;
import arduimu.ImuSensorPacket;
import arduimu.PacketVariant;
import arduimu.SensorReader;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import jssc.SerialPort;
import jssc.SerialPortException;
import logger.CsvLogger;
import logger.CsvLoggerSource;

/**
 *
 * @author Paul
 */
public class ZuptBackend implements CsvLoggerSource, SensorReader.SensorReaderDelegate
{

    SensorReader mReader;
    private int sensorID = 0;
    private String serialPortID = "COM6";
    private CsvLogger mLogger;
    private boolean isLogging;

    public static void main(String[] args)
    {
        SerialPort serialPort = new SerialPort("COM6");
        try
        {
            System.out.println("Port opened: " + serialPort.openPort());
            System.out.println("Params setted: " + serialPort.setParams(9600, 8, 1, 0));
            System.out.println("\"Hello World!!!\" successfully writen to port: " + serialPort.writeBytes("Hello World!!!".getBytes()));
            System.out.println("Port closed: " + serialPort.closePort());
        } catch (SerialPortException ex)
        {
            System.out.println(ex);
        }

    }

    public ZuptBackend()
    {
        mReader = new SensorReader();
        mLogger = new CsvLogger();
        isLogging = false;
    }

    public void startLogging(File file) throws IOException
    {
        mLogger.writeHeader(file.getPath(), file.getName(), "csv", CsvLogger.FileCollisionHandlerMode.FILENAME_NUMBER, getHeaderLine());
        isLogging = true;
        mLogger.startIOThread();
//        mReader.setOutputFile(file);
//        mLogger.startLogging(file.getPath(), file.getName(), 10, false, CsvLogger.FileCollisionHandlerMode.FILENAME_NUMBER);
    }

    public void stopLogging()
    {
        isLogging = false;
        mLogger.stopIOThread();
//        mReader.clearOutputFile();
//        mLogger.stopLogging();
    }

    public void connect()
    {
        mReader.init(serialPortID, SerialPort.BAUDRATE_115200);
        mReader.setDelegate(this);
    }

    public void connect(String serialPortID)
    {
        mReader.init(serialPortID, SerialPort.BAUDRATE_115200);
        mReader.setDelegate(this);
    }

    public void connect(String serialPortID, int baudRate)
    {
        mReader.init(serialPortID, baudRate);
        mReader.setDelegate(this);
    }

    public void disconnect()
    {
        mReader.close();
    }

    public void computePosition()
    {

    }

    @Override
    public String getHeaderLine()
    {
        return "Sample Time (millis),Sensor ID,Time (millis),AccelX (m/s^2),AccelY (m/s^2),AccelZ (m/s^2),OrientationW,OrientationX,OrientationY,OrientationZ";
    }

    @Override
    public String getDataLine()
    {
        AccelSensorPacket packet = (AccelSensorPacket) mReader.getLastPacket(sensorID, PacketVariant.ACCEL_DATA);
        ImuSensorPacket imuPacket = (ImuSensorPacket) mReader.getLastPacket(sensorID, PacketVariant.IMU_DATA);

        Date mDate = new Date();

        long sampleInstant = mDate.getTime();

        return sampleInstant + ","
                + packet.timestamp + ","
                + packet.linearAcceleration.x + ","
                + packet.linearAcceleration.y + ","
                + packet.linearAcceleration.z + ","
                + imuPacket.orientation.w + ","
                + imuPacket.orientation.x + ","
                + imuPacket.orientation.y + ","
                + imuPacket.orientation.z;
    }

    @Override
    public void didRecievepacket(BasicSensorPacket packet)
    {
        if (PacketVariant.CALIBRATION_STATUS == packet.variant)
        {
            System.out.println(packet);
        }

        if (isLogging)
        {
            if (PacketVariant.ACCEL_DATA == packet.variant)
            {
                AccelSensorPacket accelPacket = (AccelSensorPacket) packet;

                ImuSensorPacket imuPacket = (ImuSensorPacket) mReader.getLastPacket(packet.sensorID, PacketVariant.IMU_DATA);;
                long dataTime = (new Date()).getTime();

                String logData
                        = dataTime + ","
                        + packet.sensorID + ","
                        + accelPacket.timestamp + ","
                        + accelPacket.linearAcceleration.x + ","
                        + accelPacket.linearAcceleration.y + ","
                        + accelPacket.linearAcceleration.z;

                if (null != imuPacket)
                {
                    logData = logData + ","
                            + imuPacket.orientation.w + ","
                            + imuPacket.orientation.x + ","
                            + imuPacket.orientation.y + ","
                            + imuPacket.orientation.z;
                }

                try
                {
                    mLogger.addLogLine(logData);
                } catch (IOException ex)
                {
                    Logger.getLogger(ZuptBackend.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
