/*
 * The MIT License
 *
 * Copyright 2015 Paul.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package zupt.ui;

/**
 *
 * @author Paul
 */
public class ZuptCommandHandler
{

    public void handleCommand(String[] args)
    {
        try
        {
            ZuptCommand cmd = ZuptCommand.valueOf(args[0]);
            
            switch(cmd)
            {
                    
                default:
                    break;
            };
            
            
            
            
            
            
            
        } catch (IllegalArgumentException e)
        {
            help();
        }
    }

    public void help()
    {
        System.out.println("------------Commands-----------");
        for (ZuptCommand cmd : ZuptCommand.values())
        {
            System.out.println(cmd + " - args: ");
            for (String arg : cmd.possibleArguments)
            {
                System.out.print("\t" + arg);
            }
        }        
        System.out.println("-------------------------------");
    }
    
    /**
     * search a string array for a desired value and return its index.
     * return -1 if not found
     * @param array
     * @param value
     * @return index of <code>value</code> in <code>array</code>, or -1 if not found
     */
    public int indexOf(String[] array, String value)
    {
        int indexVal = -1;
        // linear search because lazy
        for (int index = 0; index < array.length; index++)
        {
            if (array[index].equals(value))
            {
                indexVal = index;
                break;
            }
        }
        
        return  indexVal;
    }
}
