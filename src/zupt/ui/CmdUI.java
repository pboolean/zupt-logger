/*
 * The MIT License
 *
 * Copyright 2015 Paul.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package zupt.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import zupt.backend.ZuptBackend;

/**
 *
 * @author Paul
 */
public class CmdUI extends Application
{

    ZuptBackend mBackend;
    private boolean keepGoing;

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        mBackend = new ZuptBackend();
        keepGoing = true;

        Thread uiThread = new Thread(cmdUIRunnable, "Main UI thread");
        uiThread.start();
        primaryStage.show();
        primaryStage.fireEvent(new WindowEvent(primaryStage, WindowEvent.WINDOW_CLOSE_REQUEST));
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);

    }

    public Runnable cmdUIRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            String inputString = "";
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));

            while (keepGoing)
            {
                try
                {
                    inputString = inputReader.readLine();
                } catch (IOException ex)
                {
                    Logger.getLogger(CmdUI.class.getName()).log(Level.SEVERE, null, ex);
                }

                String[] inputArgs = inputString.split(" ");

                switch (inputArgs[0])
                {
                    case "connect":
                        if (inputArgs.length > 1)
                        {
                            String serialPortString = inputArgs[1];
                            mBackend.connect(serialPortString);
                        } else
                        {
                            System.out.println("Connecting");
                            mBackend.connect();
                        }
                        break;

                    case "disconnect":
                        System.out.println("disconnecting");
                        mBackend.disconnect();
                        break;

                    case "startOutput":
                        String fileName = "output";
                        if (inputArgs.length > 1)
                        {
                            fileName = inputArgs[1];
                        }

                        File mFile = new File(fileName);
                        System.out.println("outputting to file '" + mFile.getAbsolutePath() + "'");
                        {
                            try
                            {
                                mBackend.startLogging(mFile);
                            } catch (IOException ex)
                            {
                                Logger.getLogger(CmdUI.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        break;

                    case "stopOutput":
                        System.out.println("Stopping output. do not quit until it is safe to do so.");
                        mBackend.stopLogging();
                        break;

                    case "quit":
                        System.err.println("Quitting");
                        mBackend.stopLogging();
                        mBackend.disconnect();
                        keepGoing = false;
                        break;

                    default:
                        break;
                }
            }
        }
    };
}
