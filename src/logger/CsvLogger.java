/*
 * The MIT License
 *
 * Copyright 2015 Paul.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paul
 */
public class CsvLogger
{

    // separate thread for logging position, independent of main thread.
    private Timer logPositionTimer;
    private String logFilePath;
    private Queue<String> logDataQueue;
    private volatile boolean isDataAvailable;

    // instrumentation
    private boolean isLogging;
    private boolean wasLogging = false;

    /**
     * has a log file been opened? (i.e. is it legit to call Resume)
     */
    private boolean primedLogFile = false;

    private CsvLoggerSource mSource;

    /**
     * Construct a CsvLogger that can run a logging thread and poll a source
     *
     * @param dataLoggerDelegate
     */
    public CsvLogger(CsvLoggerSource dataLoggerDelegate)
    {
        this.mSource = dataLoggerDelegate;
        this.isLogging = false;
    }

    /**
     * Construct a CsvLogger without a logging thread. You must the public
     * logData every time you want output
     */
    public CsvLogger()
    {
        this.isLogging = false;
    }

    /**
     * Begin logging at a specific frequency. Successive calls do nothing. Use
     * CSV file extension.
     *
     * @param outputFilePath full file path to save data to
     * @param outputFileName name of file where data will be saved
     * @param period sampling interval
     * @param overwriteExisting if a log file of this name already exists,
     * should it be overwritten, or should a new file be created?
     * @param mCollisionHandlerMode How should filename collisions be resolved?
     * @return true if logging was started, false if already logging.
     */
    public boolean startLogging(String outputFilePath, String outputFileName, int period, boolean overwriteExisting, FileCollisionHandlerMode mCollisionHandlerMode) throws IOException
    {
        return startLogging(outputFilePath, outputFileName, "csv", period, overwriteExisting, mCollisionHandlerMode);
    }

    /**
     * Begin logging at a specific frequency. Successive calls do nothing. Use
     * CSV file extension.
     *
     * @param outputFilePath full file path to save data to
     * @param outputFileName name of file where data will be saved
     * @param extension file extension for data file
     * @param period sampling interval
     * @param overwriteExisting if a log file of this name already exists,
     * should it be overwritten, or should a new file be created?
     * @param mCollisionHandlerMode How should filename collisions be resolved?
     * @return true if logging was started, false if already logging.
     */
    public boolean startLogging(String outputFilePath, String outputFileName, String extension, int period, boolean overwriteExisting, FileCollisionHandlerMode mCollisionHandlerMode) throws IOException
    {
        if (!isLogging && (null != mSource))
        {
            //isLogging = true;
            wasLogging = false;

            String logHeader = mSource.getHeaderLine();
            writeHeader(outputFilePath, outputFileName, extension, mCollisionHandlerMode, logHeader);

            primedLogFile = true;
            resumeLogging();
        } else
        {
            wasLogging = true;
        }

        return !wasLogging;
    }

    public void writeHeader(String outputFilePath, String outputFileName, String fileExtension, FileCollisionHandlerMode mCollisionHandlerMode, String logHeader) throws IOException
    {
        // set up logging
        File logOutputFile = getOutputFile(outputFilePath, outputFileName, fileExtension, 0, mCollisionHandlerMode);
        logFilePath = logOutputFile.getAbsolutePath();

        FileWriter logFileWriter = new FileWriter(logOutputFile);

        // write header
        logFileWriter.write(logHeader + "\n");
        logFileWriter.close();
    }

    private volatile boolean isIOThreadRunning = false;
    private Thread ioThread;

    /**
     * Start a thread to process incoming file write requests
     */
    public void startIOThread()
    {
        logDataQueue = new LinkedList<>();
        isIOThreadRunning = true;
        Runnable ioRunnable = new Runnable()
        {

            @Override
            public void run()
            {
                while (isIOThreadRunning)
                {

                    String logData = "";
                    while (isDataAvailable)
                    {
                        try
                        {
                            synchronized (logDataQueue)
                            {
                                logData += logDataQueue.remove() + "\n";
                                if (0 == logDataQueue.size())
                                {
                                    isDataAvailable = false;
                                }
                            }
                        } catch (Exception ex)
                        {
                            System.err.println("exception caught. continuing File IO thread." + ex);
                        }
                    }

                    try
                    {
                        writeLogLine(logData);
                    } catch (Exception ex)
                    {
                        System.err.println("exception caught. continuing File IO thread." + ex);
                    }
                    try
                    {
                        Thread.sleep(12);
                    } catch (InterruptedException ex)
                    {
                        System.err.println("Thread " + Thread.currentThread().getName() + " was woken up.\n" + ex);
                    }
                }
                System.out.println("Finished logging to file " + logFilePath);
            }
        };

        ioThread = new Thread(ioRunnable, "I/O Thread");

        ioThread.start();
    }

    public void stopIOThread()
    {
        isIOThreadRunning = false;
    }

    /**
     * Resume logging. Requires that a log file has already been setup.
     *
     * @return 'true' if currently logging. 'false' if 'startLogging' has not
     * been called
     */
    public boolean resumeLogging()
    {
        if (primedLogFile && (null != mSource))
        {
            isLogging = true;
            logPositionTimer = new Timer("CSV logging poll task");

            TimerTask loggerTimerTask = new TimerTask()
            {

                /**
                 * Get the current data from the <code>CsvLoggerSource</code>
                 * and store it in a text file, along with the time
                 */
                @Override
                public void run()
                {
                    String logData = mSource.getDataLine();
                    try
                    {
                        addLogLine(logData);
                    } catch (IOException ex)
                    {
                        Logger.getLogger(CsvLogger.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            };

            logPositionTimer.schedule(loggerTimerTask, 50, 5); // start the timer with a 16ms interval, delay the start to ignore any blips at the beginning        
        }

        return primedLogFile && isLogging;

    }

    /**
     * enumerate the different possible ways to handle filename collisions
     */
    public enum FileCollisionHandlerMode
    {

        /**
         * duplicated files will have a version_number suffix, i.e.
         * &lt;Folder&gt;/&lt;Filename&gt;_vers_1.&lt;extension&gt;
         */
        FILENAME_VERSION_NUMBER,
        /**
         * duplicated files will be placed in a sibling folder with a
         * version_number suffix, i.e.
         * &lt;Folder&gt;_vers_1/&lt;Filename&gt;.&lt;extension&gt;
         */
        FOLDERNAME_VERSION_NUMBER,
        /**
         * duplicated files will be placed in a sibling folder with a number
         * suffix, i.e. &lt;Folder&gt;_1/&lt;Filename&gt;.&lt;extension&gt;
         */
        FILENAME_NUMBER,
        /**
         * duplicated files will have a number suffix, i.e.
         * &lt;Folder&gt;/&lt;Filename&gt;_1.&lt;extension&gt;
         */
        FOLDERNAME_NUMBER
    }

    /**
     * Get a file pointer that can be used for data logging, and ensure that
     * duplicate files are preserved.
     *
     * @param filePath the path to try
     * @param fileName name of the output file
     * @param extension file extension (no '.', e.g. for a .csv file this would
     * be 'csv')
     * @param existing the number of times that this had already failed
     * @param mCollisionHandlerMode How should filename collisions be resolved?
     * @return a <code>FileWriter</code> that can be used to output text data,
     * or null if the <code>FileWriter</code> could not be created
     */
    public static File getOutputFile(String filePath, String fileName, String extension, int existing, FileCollisionHandlerMode mCollisionHandlerMode)
    {
        File outputFile;
        String decoratedPath = filePath;
        String decoratedName = fileName;

        switch (mCollisionHandlerMode)
        {
            case FILENAME_VERSION_NUMBER:
                if (existing != 0)
                {
                    decoratedName += "_vers_" + existing;
                }
                break;

            case FILENAME_NUMBER:
                if (existing != 0)
                {
                    decoratedName += "_" + existing;
                }
                break;

            case FOLDERNAME_VERSION_NUMBER:
                if (existing != 0)
                {
                    decoratedPath += "_vers_" + existing;
                }
                break;

            case FOLDERNAME_NUMBER:
                if (existing != 0)
                {
                    decoratedPath += "_" + existing;
                }
                break;

            default:
                throw new IllegalArgumentException(mCollisionHandlerMode + " is not a recognized CollisionHandler");
        }

        String directory = decoratedPath;

        decoratedPath += "/" + decoratedName + "." + extension;

        File outDir = new File(directory);
        outDir.mkdirs();

        File outFile = new File(decoratedPath);
        if (outFile.exists())
        {  // file already exists, recursively make a new file
            outputFile = getOutputFile(filePath, fileName, extension, existing + 1, mCollisionHandlerMode);
        } else
        {
            outputFile = outFile;
        }

        return outputFile;
    }

    /**
     * stop logging. Successive calls do nothing.
     *
     * @return
     */
    public boolean stopLogging()
    {
        boolean didStop = haltLogging();
        primedLogFile = false;
        return didStop;
    }

    /**
     * Pause logging (may resume)
     *
     * @return 'true' if logging was stopped, false if it was previously
     * stopped.
     */
    public boolean haltLogging()
    {
        if (isLogging && (null != mSource))
        {
            isLogging = false;
            wasLogging = true;
            logPositionTimer.cancel();
        } else
        {
            wasLogging = false;
        }

        //Debug.Log("Stopped logging position to file " + logFilePath);
        return wasLogging;
    }

    /**
     * Write a single line to the log file.
     *
     * @param logData a line of comma-separated text to print to the log file.
     * should not be new-line terminated!
     * @return
     */
    public void addLogLine(String logData) throws IOException
    {
        try
        {
            synchronized (logDataQueue)
            {
                logDataQueue.add(logData);
                isDataAvailable = true;
            }
        } catch (IllegalStateException ise)
        {
            System.err.println(ise);
        }
    }

    private void writeLogLine(String logData) throws IOException
    {
        FileWriter dataWriter = null;
        

        dataWriter = new FileWriter(logFilePath, true); // append to a file that exists
        dataWriter.append(logData);
        dataWriter.close();
    }

}
